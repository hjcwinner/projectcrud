const express = require('express') //새로운파일은 무조건 불러와야되는듯
const router = express.Router(); ///라우터 함수로 접근
const bodyParser = require('body-parser')
const productModel = require('../model/product')

/////불러오기get
router.get('/', (req, res) => {
    productModel
        .find()
        .then(docs => {
            res.json({
                message : "전체보기",
                proInfo : docs
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

router.get('/:pid', (req, res) => {
    productModel
        .findById(req.params.pid)
        .then(doc => {
            res.json({
                message : "한개보기",
                proInfo : doc
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

/////등록하기post
router.post('/', (req, res) => {
    const product = new productModel({
        name : req.body.namefive,
        price : req.body.pricefive
    })
    product
        .save()
        .then(prosave => {
            res.json({
                message : "저장완료",
                productInfo : prosave
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});
/////수정하기patch
router.patch('/', (req, res) => {
    res.json({
        message : "수정하기"
    });
});
/////삭제하기delete
router.delete('/:pid', (req, res) => {
    productModel
        .findByIdAndDelete(req.params.pid)
        .then(docdel => {
            res.json({
                message : "삭제하기"
            })
        })
        .catch(err => {
            res.json({
                message : err.message
            })
        })
});

module.exports = router;