const mongoose = require('mongoose')
const productModel = mongoose.Schema({
    name : String,
    price : Number
})

module.exports = mongoose.model('product',productModel)